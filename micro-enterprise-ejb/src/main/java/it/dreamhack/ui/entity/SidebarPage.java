package it.dreamhack.ui.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class SidebarPage implements Page, Serializable {
    @Id
    String name;
    String label;
    String iconUri;
    String uri;
    String permission;

    public SidebarPage(String name, String label, String iconUri, String uri) {
        super();
        this.name = name;
        this.label = label;
        this.iconUri = iconUri;
        this.uri = uri;
    }

    public SidebarPage() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIconUri() {
        return iconUri;
    }

    public void setIconUri(String iconUri) {
        this.iconUri = iconUri;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
