package it.dreamhack.ui.entity;

public interface Page {
    public String getUri();
    public String getPermission();
    public String getName();
}
