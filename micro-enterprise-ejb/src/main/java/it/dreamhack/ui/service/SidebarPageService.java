package it.dreamhack.ui.service;

import it.dreamhack.ui.entity.SidebarPage;

import javax.ejb.Local;
import java.util.List;

@Local
public interface SidebarPageService {
    List<SidebarPage> getSidebarPages();
    SidebarPage getPage(String p);
}
