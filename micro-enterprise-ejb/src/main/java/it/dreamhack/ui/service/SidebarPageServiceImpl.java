package it.dreamhack.ui.service;

import it.dreamhack.ui.entity.SidebarPage;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Stateless
public class SidebarPageServiceImpl implements SidebarPageService {
    @PersistenceContext
    EntityManager em;

    Map<String, SidebarPage> pages = new HashMap<>();

    //TODO: read from database with EntityManager, instead of creating SidebarPage.
    public List<SidebarPage> getSidebarPages() {
//        pages.put("zk", new SidebarPage("zk","ZK","/imgs/site.png","http://www.zkoss.org/"));
//        pages.put("demo", new SidebarPage("demo","ZK Demo","/imgs/demo.png","http://www.zkoss.org/zkdemo"));
//        pages.put("devref", new SidebarPage("devref","ZK Developer Reference","/imgs/doc.png","http://books.zkoss.org/wiki/ZK_Developer's_Reference"));
        pages.put("user_index", new SidebarPage("user_index", "Users", "/imgs/profile.png", "/pages/users/index.zul"));

        return new ArrayList<>(pages.values());
    }

    @Override
    public SidebarPage getPage(String p) {
        return pages.get(p);
    }
}
