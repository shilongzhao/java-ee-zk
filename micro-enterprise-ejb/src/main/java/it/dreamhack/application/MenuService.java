package it.dreamhack.application;

import javax.ejb.Local;
import java.util.List;

@Local
public interface MenuService {
    List<Menu> getAuthorizedMenus(Long userId, Long appId);
}
