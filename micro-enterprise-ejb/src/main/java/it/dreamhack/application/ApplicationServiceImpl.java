package it.dreamhack.application;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class ApplicationServiceImpl implements ApplicationService {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Application> getApplications() {
        return em.createQuery(
                "SELECT a FROM Application a", Application.class)
                .getResultList();
    }

    @Override
    public List<Application> getAuthorizedApplications(Long userId) {
        return getApplications();
    }
}
