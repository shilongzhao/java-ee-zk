package it.dreamhack.application;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class MenuServiceImpl implements MenuService {
    @PersistenceContext
    private EntityManager em;
    /**
     * Get authorized menus for userId of Application appId
     * @param userId
     * @param appId
     * @return
     */
    @Override
    public List<Menu> getAuthorizedMenus(Long userId, Long appId) {
        return em.createQuery(
                "SELECT m " +
                        "   FROM Menu m " +
                        "       JOIN FETCH m.application a " +
                        "WHERE a.id = :id", Menu.class)
                .setParameter("id", appId)
                .getResultList();
    }
}
