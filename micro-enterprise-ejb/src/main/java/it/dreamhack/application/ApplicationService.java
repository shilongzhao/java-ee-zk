package it.dreamhack.application;

import javax.ejb.Local;
import java.util.List;

@Local
public interface ApplicationService {
    public List<Application> getApplications();

    List<Application> getAuthorizedApplications(Long userId);
}
