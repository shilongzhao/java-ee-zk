package it.dreamhack.user.exception;

/**
 * author: zhaoshilong
 * date: 21/10/2017
 */
public class UserException extends Exception {

    public UserException(String msg) {
        super(msg);
    }
}
