package it.dreamhack.user.service.impl;

import it.dreamhack.user.entity.User;
import it.dreamhack.user.exception.UserException;
import it.dreamhack.user.service.UserService;
import org.apache.shiro.authc.credential.PasswordService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * author: zhaoshilong
 * date: 15/08/2017
 */
@Stateless
public class UserServiceImpl implements UserService {
    @PersistenceContext
    private EntityManager em;
    @Inject
    private PasswordService passwordService;

    @Override
    public void create(User user) throws UserException {
        User u = getUserByUsername(user.getUsername());
        if (u != null) {
            throw new UserException("Username already exists");
        }
        String encryptedPassword = passwordService.encryptPassword(user.getPassword());
        user.setPassword(encryptedPassword);
        em.persist(user);
    }

    @Override
    public User getUserById(Long userId) {
        List<User> users =  em.createQuery(
                "SELECT u " +
                        "FROM User u " +
                        "WHERE u.id = :userId", User.class)
                .setParameter("userId", userId)
                .getResultList();
        if (users.size() == 0) {
            return null;
        }
        return users.get(0);
    }

    public User getUserByUsername(String username) {
        List<User> users =  em.createQuery(
                "SELECT u " +
                        "FROM User u " +
                        "WHERE u.username = :username", User.class)
                .setParameter("username", username)
                .getResultList();
        if (users.size() == 0) {
            return null;
        }
        return users.get(0);

    }

    @Override
    public List<User> getUsers(int offset, int limit) {
        return em.createQuery("SELECT u FROM User u", User.class)
                .setFirstResult(offset)
                .setMaxResults(limit).getResultList();
    }

    public Set<String> getUserRoleStrings(String username) {
        List<String> roles = em.createQuery(
                "SELECT DISTINCT r.name " +
                        "FROM User u " +
                        "   LEFT JOIN UserRole ur " +
                        "   LEFT JOIN Role r " +
                        "WHERE u.username = :username " , String.class)
                .setParameter("username", username)
                .getResultList();

        return new HashSet<>(roles);

    }

    public Set<String> getUserPermissionStrings(String username) {
        List<String> perms = em.createQuery(
                "SELECT DISTINCT p.code " +
                        "FROM User u " +
                        "   LEFT JOIN UserRole ur " +
                        "   LEFT JOIN Role r " +
                        "   LEFT JOIN RolePermission rp " +
                        "   LEFT JOIN Permission  p " +
                        "WHERE u.username = :username", String.class)
                .setParameter("username", username)
                .getResultList();

        return new HashSet<>(perms);
    }

    @Override
    public Long countUsers() {
        return em.createQuery("SELECT COUNT(u) FROM User u", Long.class).getSingleResult();
    }

    @Override
    public List<String> getUserRoles(String username) {
        List<String> roles = em.createQuery(
                " SELECT DISTINCT r.name " +
                        " FROM UserRole ur " +
                        "   JOIN FETCH ur.user u " +
                        "   JOIN FETCH ur.role r " +
                        " WHERE " +
                        "   u.username = :username ", String.class)
                .setParameter("username", username)
                .getResultList();
        return roles;
    }
}
