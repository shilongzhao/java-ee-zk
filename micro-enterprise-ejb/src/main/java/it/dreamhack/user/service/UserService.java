package it.dreamhack.user.service;

import it.dreamhack.user.entity.User;
import it.dreamhack.user.exception.UserException;

import javax.ejb.Local;
import java.util.List;
import java.util.Set;

/**
 * author: zhaoshilong
 * date: 15/08/2017
 */
@Local
public interface UserService {
    void create(User user) throws UserException;
    User getUserById(Long selectedUserId);
    User getUserByUsername(String username);
    List<User> getUsers(int offset, int limit);
    Set<String> getUserRoleStrings(String username);
    Set<String> getUserPermissionStrings(String username);
    Long countUsers();
    List<String> getUserRoles(String username);
}
