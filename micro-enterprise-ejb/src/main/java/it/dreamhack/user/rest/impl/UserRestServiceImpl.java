package it.dreamhack.user.rest.impl;

import it.dreamhack.user.dto.UserDto;
import it.dreamhack.user.entity.User;
import it.dreamhack.user.rest.UserRestService;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class UserRestServiceImpl implements UserRestService{
    @PersistenceContext
    private EntityManager em;
    @Override
    public Response getUsers() {
        List<User> users = em.createQuery("SELECT u FROM User u", User.class).getResultList();
        List<UserDto> userDtos = users.stream().map(user -> new UserDto(user)).collect(Collectors.toList());
        return Response.ok(userDtos).build();
    }
}
