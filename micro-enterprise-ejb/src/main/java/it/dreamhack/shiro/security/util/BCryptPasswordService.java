package it.dreamhack.shiro.security.util;

import org.apache.shiro.authc.credential.PasswordService;
import org.mindrot.jbcrypt.BCrypt;

import java.util.logging.Logger;

public class BCryptPasswordService implements PasswordService {
    private static final Logger log = Logger.getLogger(BCryptPasswordService.class.getCanonicalName());

    public static final int DEFAULT_BCRYPT_ROUND = 10;

    public BCryptPasswordService() {
        super();
    }

    @Override
    public String encryptPassword(Object plaintextPassword) {
        if (plaintextPassword instanceof String) {
            String password = (String) plaintextPassword;
            return BCrypt.hashpw(password, BCrypt.gensalt(DEFAULT_BCRYPT_ROUND));
        }
        throw new IllegalArgumentException(
                "BCryptPasswordService encryptPassword only support java.lang.String credential.");
    }

    @Override
    public boolean passwordsMatch(Object submittedPlaintext, String encrypted) {
        if (submittedPlaintext instanceof char[]) {
            String password = String.valueOf((char[]) submittedPlaintext);
            return BCrypt.checkpw(password, encrypted);
        }
        throw new IllegalArgumentException(
                "BCryptPasswordService passwordsMatch only support char[] credential.");
    }
}
