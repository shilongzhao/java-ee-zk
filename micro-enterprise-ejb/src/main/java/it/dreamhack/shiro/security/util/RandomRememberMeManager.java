package it.dreamhack.shiro.security.util;

import org.apache.shiro.crypto.AesCipherService;
import org.apache.shiro.web.mgt.CookieRememberMeManager;

public class RandomRememberMeManager extends CookieRememberMeManager {
    public RandomRememberMeManager() {
        super();
        AesCipherService aesCipherService = new AesCipherService();
        setCipherService(aesCipherService);
        setCipherKey(aesCipherService.generateNewKey().getEncoded());
    }
}
