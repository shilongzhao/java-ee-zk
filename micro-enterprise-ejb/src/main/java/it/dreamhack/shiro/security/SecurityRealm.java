package it.dreamhack.shiro.security;

import it.dreamhack.user.entity.User;
import it.dreamhack.user.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.naming.InitialContext;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

public class SecurityRealm extends AuthorizingRealm {
    private static final Logger log = Logger.getLogger(SecurityRealm.class.getCanonicalName());
    private UserService userService;

    public SecurityRealm() {
        super();
        try {
            InitialContext context = new InitialContext();
            String moduleName = (String) context.lookup("java:module/ModuleName");
            String appName = (String) context.lookup("java:app/AppName");
            this.userService = (UserService) context.lookup(String.format("java:global/%s/%s/UserServiceImpl", appName, moduleName));
        } catch (Exception ex) {
            log.warning("Cannot do the JNDI lookup to instantiate the UserService: " + ex);
        }
    }
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        if (principals == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }

        String username = (String) getAvailablePrincipal(principals);
        List<String> roleNames =  userService.getUserRoles(username);
        return new SimpleAuthorizationInfo(new HashSet<>(roleNames));
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken userPassToken = (UsernamePasswordToken) token;
        String username = userPassToken.getUsername();
        if (username == null) {
            log.warning("Username is null.");
            return null;
        }
        User user = userService.getUserByUsername(username);
        if (user == null) {
            log.warning("No account found for user [" + username + "]");
            throw new IncorrectCredentialsException();
        }
        return new SimpleAuthenticationInfo(username, user.getPassword(), getName());
    }
}
