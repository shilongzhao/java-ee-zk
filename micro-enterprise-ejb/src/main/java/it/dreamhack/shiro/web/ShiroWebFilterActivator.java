package it.dreamhack.shiro.web;

import org.apache.shiro.web.servlet.ShiroFilter;

import javax.servlet.annotation.WebFilter;

@WebFilter("/*")
public class ShiroWebFilterActivator extends ShiroFilter {
    private ShiroWebFilterActivator() {

    }
}
