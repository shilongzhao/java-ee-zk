package it.dreamhack.shiro;

import it.dreamhack.shiro.security.SecurityRealm;
import it.dreamhack.shiro.security.util.BCryptPasswordService;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.FilterChainManager;
import org.apache.shiro.web.filter.mgt.FilterChainResolver;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.mgt.WebSecurityManager;

import it.dreamhack.shiro.security.util.RandomRememberMeManager;

import javax.enterprise.inject.Produces;

public class SecurityConfiguration {

    @Produces
    public WebSecurityManager getSecurityManager() {
        AuthorizingRealm realm = new SecurityRealm();

        CredentialsMatcher credentialsMatcher = new PasswordMatcher();
        ((PasswordMatcher) credentialsMatcher).setPasswordService(new BCryptPasswordService());
        realm.setCredentialsMatcher(credentialsMatcher);

        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager(realm);

        CacheManager cacheManager = new EhCacheManager();
        ((EhCacheManager) cacheManager).setCacheManagerConfigFile("classpath:ehcache.xml");
        securityManager.setCacheManager(cacheManager);

        CookieRememberMeManager rememberMeManager = new RandomRememberMeManager();

        securityManager.setRememberMeManager(rememberMeManager);

        return securityManager;
    }

    @Produces
    public FilterChainResolver getFilterChainResolver() {
        PathMatchingFilterChainResolver resolver = new PathMatchingFilterChainResolver();
        FilterChainManager fcMan = new DefaultFilterChainManager();
        //TODO: add filters
        resolver.setFilterChainManager(fcMan);
        return resolver;
    }


}
