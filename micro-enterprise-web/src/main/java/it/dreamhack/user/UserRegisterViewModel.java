package it.dreamhack.user;

import it.dreamhack.user.entity.User;
import it.dreamhack.user.exception.UserException;
import it.dreamhack.user.service.UserService;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;

import javax.inject.Inject;
import java.util.logging.Logger;

/**
 * author: zhaoshilong
 * date: 21/10/2017
 * Remember to use SwiftBindComposer
 */
public class UserRegisterViewModel {
    private static final Logger log = Logger.getLogger(UserRegisterViewModel.class.getCanonicalName());
    private User user;
    @Inject
    private UserService userService;
    private String serverResponse;
    private boolean error = false;
    private String errorMsg;
    @Init
    public void init() {
        log.info("Initializing ... ");
    }
    @AfterCompose
    public void afterCompose() {
        user = new User();
    }

    @Command("register")
    public void register() {
        try {
            userService.create(user);
        } catch (UserException e) {
            log.warning("User already exists " + user);
            serverResponse = e.getMessage();
            return;
        }
        log.info("User created " + user.getUsername());
        //TODO: redirect
    }
    @Command("usernameCheck") @NotifyChange("*")
    public void usernameCheck() {
        if (userService.getUserByUsername(user.getUsername()) != null) {
            error = true;
            errorMsg = "Username has already been taken. Please choose another one. ";
        }
        else {
            error = false;
        }
    }
    @Command("emailCheck") @NotifyChange("*")
    public void emailCheck() {
        if (user.getEmail() == null || !user.getEmail().matches("^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+") ) {
            error = true;
        }
        else {
            error |= false;
        }
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
