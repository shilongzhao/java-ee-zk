package it.dreamhack.user.vm;

import it.dreamhack.ui.SwiftDesktopAttributes;
import it.dreamhack.ui.SwiftInjectionUtil;
import it.dreamhack.ui.SwiftMainViewModel;
import it.dreamhack.ui.entity.SidebarPage;
import it.dreamhack.user.entity.User;
import it.dreamhack.user.service.UserService;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.*;
import org.zkoss.zk.ui.Executions;

import javax.inject.Inject;
import java.util.*;
import java.util.logging.Logger;

public class UserListViewModel {
    private static final Logger log = Logger.getLogger(UserListViewModel.class.getCanonicalName());
    @Inject
    private UserService userService;
    private List<User> users;
    private User queriedUser;
    @Init
    public void init() {
        SwiftInjectionUtil.inject(this);
    }

    @AfterCompose
    public void afterCompose() {
        queriedUser = new User();
        users = new ArrayList<>();
    }

    @Command @NotifyChange("users")
    public void search() {
        users.clear();
        // TODO: do real search
        users.addAll(userService.getUsers(0, 10));
    }

    @Command
    public void openDetail(@BindingParam("user") User user) {
        // TODO: redirect to user detail page and show specific information
        SwiftMainViewModel mainViewModel = (SwiftMainViewModel) Executions.getCurrent().getDesktop()
                        .getAttribute(SwiftDesktopAttributes.SWIFT_MAIN_VIEW_MODEL);
        Map<String, Object> params = new HashMap<>();
        params.put("object", user);
        Executions.getCurrent().getDesktop().setAttribute("params", params);
        // TODO: read from data base the page uri
        SidebarPage page = new SidebarPage("user_detail", "user detail", "", "/pages/users/detail.zul");
        BindUtils.postGlobalCommand(null,  null, "onNavigate", Collections.singletonMap("page", page));
    }
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public User getQueriedUser() {
        return queriedUser;
    }

    public void setQueriedUser(User queriedUser) {
        this.queriedUser = queriedUser;
    }
}
