package it.dreamhack.user.vm;

import it.dreamhack.ui.SwiftInjectionUtil;
import it.dreamhack.user.entity.User;
import it.dreamhack.user.exception.UserException;
import it.dreamhack.user.service.UserService;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Messagebox;

import javax.inject.Inject;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserProfileViewModel {
    private static final Logger log = Logger.getLogger(UserProfileViewModel.class.getCanonicalName());
    private User currentUser;
    private String msg;

    @Inject
    private UserService us;

    @Init
    public void init() {
        // TODO: set user info during login
        Map<String, Object> params = (Map<String, Object>) Executions.getCurrent().getDesktop().getAttribute("params");
        SwiftInjectionUtil.inject(this);
    }

    @AfterCompose
    public void afterCompose() {
        if (us == null) {
            log.log(Level.WARNING, "UserService is null");
            throw new RuntimeException();
        }
        currentUser = us.getUserById(1L);
        if (currentUser == null) {
            currentUser = new User();
        }
    }

    @Command @NotifyChange("*")
    public void save() {
        try {
            us.create(currentUser);
        } catch (UserException e) {
            e.printStackTrace();
        }
        Messagebox.show("User saved");
    }

    public boolean isReadonly() {
        return currentUser.getId() != null;
    }

    @Command
    @NotifyChange("currentUser")
    public void reload(){
        String username = currentUser.getUsername();
        currentUser = us.getUserByUsername(username);
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
