package it.dreamhack.ui;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.ext.AfterCompose;

import java.util.logging.Level;
import java.util.logging.Logger;

public class SwiftBookmarkManager {
    private static final Logger log = Logger.getLogger(SwiftBookmarkManager.class.getCanonicalName());
    public SwiftBookmarkManager() {
        log.log(Level.INFO, "constructing ... ");
        Executions.getCurrent().getDesktop().setAttribute(SwiftDesktopAttributes.SWIFT_BOOKMARK_MANAGER, this);
    }
    public void onNavigate() {

    }
}
