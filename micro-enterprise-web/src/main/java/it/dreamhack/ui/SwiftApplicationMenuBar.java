package it.dreamhack.ui;

import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Menubar;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * we should add self to desktop attribute in beforeCompose
 * and get desktop in afterCompose, make sure the correct steps
 */
public class SwiftApplicationMenuBar extends Menubar implements AfterCompose {
    private static final Logger log = Logger.getLogger(SwiftApplicationMenuBar.class.getCanonicalName());

    @Override
    public void afterCompose() {
        log.log(Level.INFO, "after compose");
    }

    public SwiftApplicationMenuBar() {
        super();
        log.log(Level.INFO, "constructing");
    }

    private Long appId;

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }
}

