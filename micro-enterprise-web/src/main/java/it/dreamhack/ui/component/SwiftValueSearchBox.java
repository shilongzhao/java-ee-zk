package it.dreamhack.ui.component;

import it.dreamhack.ui.SwiftInjectionUtil;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;

public abstract class SwiftValueSearchBox extends Div implements AfterCompose {
    public SwiftValueSearchBox() {
        SwiftInjectionUtil.inject(this);
    }
    @Override
    public void afterCompose() {

    }
}
