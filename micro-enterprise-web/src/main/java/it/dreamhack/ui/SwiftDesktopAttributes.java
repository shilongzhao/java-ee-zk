package it.dreamhack.ui;

public class SwiftDesktopAttributes {
    public static final String SWIFT_DESKTOP_VIEW_MODEL = "DESKTOP_VM";
    public static final String SWIFT_MAIN_VIEW_MODEL = "MAIN_VM";
    public static final String SWIFT_MAIN_MENU_BAR = "MAIN_MENU_BAR";
    public static final String SWIFT_BOOKMARK_MANAGER  = "BOOKMARK_MANAGER";
    public static final String SWIFT_NAV_BAR = "SWIFT_NAV_BAR" ;
}
