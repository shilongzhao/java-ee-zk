package it.dreamhack.ui;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SwiftInjectionUtil {
    private static final Logger log = Logger.getLogger(SwiftInjectionUtil.class.getCanonicalName());

    // TODO: make it runtime configured
    private static final String EJB_MODULE_NAME = "micro-enterprise-ejb";

    public static void inject(Object o) {
        Class<?> c = o.getClass();
        while (!Object.class.equals(c)) {
            inject(c, o);
            c = c.getSuperclass();
        }
    }
    private static void inject(Class<?> c, Object o) {
        for (Field f: c.getDeclaredFields()) {
            Class<?> fieldType = f.getType();
            if (f.isAnnotationPresent(Inject.class)) {
                Object ejb = getEjbFromJndiByType(fieldType);
                if (!f.isAccessible()) {
                    f.setAccessible(true);
                }
                try {
                    f.set(o, ejb);
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Unable to set field " + f, e);
                    throw new RuntimeException();
                }
            }
        }
    }

    private static Object getEjbFromJndiByType(Class<?> type) {
        String simpleName = type.getSimpleName();
        String packageName = type.getPackage().getName();
        return getEjbFromJndiByName(packageName, simpleName, type.isInterface());
    }

    private static Object getEjbFromJndiByName(String packageName, String simpleName, boolean isInterface) {
        Object ejb = null;
        String jndiName = null;

        try {
            InitialContext initialContext = new InitialContext();
            String appName = (String) initialContext.lookup("java:app/AppName");
            String moduleName = (String) initialContext.lookup("java:module/ModuleName");
            if (isInterface) {
                jndiName = String.format("java:global/%s/%s/%sImpl!%s.%s", appName, EJB_MODULE_NAME, simpleName, packageName, simpleName);
            } else {
                jndiName = String.format("java:global/%s/%s/%sImpl", appName, EJB_MODULE_NAME, simpleName);
            }
            ejb = initialContext.lookup(jndiName);
        } catch (NamingException e) {
            log.log(Level.SEVERE, "Unable to lookup JNDI name " + jndiName);
            throw new RuntimeException();
        }
        return ejb;
    }
}
