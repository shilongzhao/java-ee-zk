package it.dreamhack.ui;

import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zk.ui.ext.BeforeCompose;

import org.zkoss.zul.Hbox;

import java.util.logging.Level;
import java.util.logging.Logger;


public class SwiftNavigationBar extends Hbox implements AfterCompose, BeforeCompose {
    private static final Logger log = Logger.getLogger(SwiftNavigationBar.class.getCanonicalName());

    @Override
    public void afterCompose() {
        log.log(Level.INFO, "after compose");
    }

    @Override
    public void beforeCompose() {
        log.log(Level.INFO, "before compose");
    }

    public SwiftNavigationBar() {
        log.log(Level.INFO, "constructing ... ");
    }
}
