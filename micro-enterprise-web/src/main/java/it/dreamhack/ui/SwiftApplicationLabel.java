package it.dreamhack.ui;

import it.dreamhack.application.Application;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;

import java.util.logging.Level;
import java.util.logging.Logger;

public class SwiftApplicationLabel extends Label implements AfterCompose {
    private static final Logger log = Logger.getLogger(SwiftApplicationLabel.class.getCanonicalName());
    private Long appId;
    private String appName;
    private boolean selected;

    @Override
    // NOT Called
    public void afterCompose() {
        log.log(Level.INFO, "after compose");
    }

    public SwiftApplicationLabel() {
        super();
        log.log(Level.INFO, "constructing");
    }
    // NOT Called

    public SwiftApplicationLabel(Application app) {
        super();
        this.appId = app.getId();
        this.appName = app.getDisplayName();
        setValue(app.getDisplayName());
        log.log(Level.INFO, "constructing");

    }
    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


}
