package it.dreamhack.ui;

import it.dreamhack.ui.entity.SidebarPage;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;

public class SwiftMainViewModel {
    private String includeSrc = "/templates/main.zul";

    @Init
    public void init() {
        SwiftInjectionUtil.inject(this);
        Executions.getCurrent().getDesktop().setAttribute(SwiftDesktopAttributes.SWIFT_MAIN_VIEW_MODEL, this);
    }

    @GlobalCommand("onNavigate")
    @NotifyChange("includeSrc")
    public void onNavigate(@BindingParam("page")SidebarPage page) {
        String locationUri = page.getUri();
        String name = page.getName();
        //redirect current url to new location
        if(locationUri.startsWith("http")){
            //open a new browser tab
            Executions.getCurrent().sendRedirect(locationUri);
        } else {
            // here the center view is updated
            includeSrc = locationUri;

            //advance bookmark control,
            //bookmark with a prefix
            if(name!=null){
                Executions.getCurrent().getDesktop().setBookmark("p_"+name);
            }
        }
    }

    public String getIncludeSrc() {
        return includeSrc;
    }
}
