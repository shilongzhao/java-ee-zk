package it.dreamhack.ui;

import org.zkoss.bind.BindComposer;
import org.zkoss.zk.ui.Component;

/**
 * Binder that do injection, so the ViewModel does not have to do injection by itself
 * @param <T>
 */
public class SwiftBindComposer<T extends Component> extends BindComposer<Component> {
    @Override
    public void doBeforeComposeChildren(Component comp) throws Exception {
        super.doBeforeComposeChildren(comp);
        SwiftInjectionUtil.inject(super.getViewModel());
    }
}
