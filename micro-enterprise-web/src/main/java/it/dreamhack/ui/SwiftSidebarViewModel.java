package it.dreamhack.ui;

import it.dreamhack.ui.entity.SidebarPage;
import it.dreamhack.ui.service.SidebarPageService;
import org.zkoss.bind.annotation.Init;

import javax.inject.Inject;
import java.util.List;

public class SwiftSidebarViewModel {

    @Inject
    private SidebarPageService sidebarPageService;
    @Init
    public void init() {
        SwiftInjectionUtil.inject(this);
    }

    public List<SidebarPage> getSidebarPages() {
        return sidebarPageService.getSidebarPages();
    }

}
