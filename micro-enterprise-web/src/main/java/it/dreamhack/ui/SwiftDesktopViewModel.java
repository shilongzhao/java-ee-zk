package it.dreamhack.ui;

import it.dreamhack.application.Application;
import it.dreamhack.application.ApplicationService;
import it.dreamhack.application.Menu;
import it.dreamhack.application.MenuService;
import org.zkoss.bind.annotation.*;

import javax.inject.Inject;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.metainfo.LanguageDefinition;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.*;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class SwiftDesktopViewModel {

    private static Logger log = Logger.getLogger(SwiftDesktopViewModel.class.getCanonicalName());

    @Inject
    private ApplicationService applicationService;
    @Inject
    private MenuService menuService;

    @Wire("#swiftNavigationBar")
    private SwiftNavigationBar navigationBar;

    @Wire("#swiftApplicationMenuBar")
    private SwiftApplicationMenuBar menuBar;

    @Wire("#tabBox")
    private Tabbox tabbox;

    private SwiftApplicationLabel selectedApp;

    private SwiftBookmarkManager bookmarkManager;

    public SwiftDesktopViewModel() {
        super();
        log.log(Level.INFO, "constructing ...");
    }
    @Init
    public void init() {
        log.log(Level.INFO, "initializing...");
        SwiftInjectionUtil.inject(this);
//        Executions.getCurrent().getDesktop().setAttribute(SwiftDesktopAttributes.SWIFT_DESKTOP_VIEW_MODEL, this);
    }
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        log.log(Level.INFO, "after compose");
        Selectors.wireComponents(view, this, false);
        renderNavigationBar();
        renderMenuBar();
    }

    private void renderNavigationBar() {
        List<Application> apps = applicationService.getAuthorizedApplications(1L);
        for (Application app: apps) {
            SwiftApplicationLabel label = newComponentInstance("swift-application-label", SwiftApplicationLabel.class);
            label.setParent(navigationBar);
            label.setValue(app.getDisplayName());
            label.setAppId(app.getId());
            label.addEventListener("onClick", event -> setSelectedApp(label));
        }
    }

    private <T extends Component> T newComponentInstance(String componentName, Class<T> componentClass) {
        LanguageDefinition def = LanguageDefinition.lookup(null);
        Component c = def.getComponentDefinition(componentName).newInstance(componentClass);
        c.applyProperties();
        return (T) c;
    }

    private void renderMenuBar() {
        renderMenus(1L);
    }



    private void navigate(Menu menu) {
        navigate(menu.getLink(), null);
    }

    private void navigate(String toZul, Map<String, Object> params) {
        Tabs tabs = tabbox.getTabs();
        Tab tab = new Tab();
        tab.setLabel("tabName");
        tab.setSelected(true);
        tab.setClosable(true);
        tab.setParent(tabs);

        Tabpanels tabpanels = tabbox.getTabpanels();
        Tabpanel tabpanel = new Tabpanel();
        tabpanel.setParent(tabpanels);

        Include include = new Include();
        include.setParent(tabpanel);
        include.setMode("defer");
        include.setSrc(toZul);
    }

    public SwiftApplicationLabel getSelectedApp() {
        return selectedApp;
    }

    private void setSelectedApp(SwiftApplicationLabel selectedApp) {
        this.selectedApp = selectedApp;
        renderMenus(selectedApp.getAppId());
    }

    /**
     * render menus of an application
     * @param appId application ID
     */
    private void renderMenus(Long appId) {
        menuBar.getChildren().clear();

        log.log(Level.INFO, "rendering menu for application " + appId);
        Long userId = 1L;
        List<Menu> authorizedMenus = menuService.getAuthorizedMenus(userId, appId);
        authorizedMenus.stream()
                .filter(menu -> menu.getParentMenu() == null)
                .forEach(topLevelMenu -> build(topLevelMenu, menuBar, authorizedMenus));
    }

    /**
     * build a hierarchical menu structure whose root is in component
     * @param parent the root menus' (first level menus) parent
     * @param menus list of menus to handle
     */
    private void build(Menu menu, Component parent, List<Menu> menus) {
        List<Menu> children = menus.stream()
                .filter(m -> m.getParentMenu() != null && m.getParentMenu().equals(menu))
                .collect(Collectors.toList());
        if (children.isEmpty()) {
            SwiftApplicationMenuItem menuItem = new SwiftApplicationMenuItem();
            menuItem.setLabel(menu.getName());
            menuItem.setParent(parent);
            menuItem.addEventListener("onClick", event -> navigate(menu));
        } else {
            SwiftApplicationMenu menuUi = new SwiftApplicationMenu();
            menuUi.setLabel(menu.getName());
            menuUi.setParent(parent);
            Menupopup menupopup = new Menupopup();
            menupopup.setParent(menuUi);
            for (Menu m : children) {
                build(m, menupopup, menus);
            }
        }
    }

}
